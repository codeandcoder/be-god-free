package com.wordpress.rasanti.begodfree.ai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.wordpress.rasanti.begodfree.BeGodGame;
import com.wordpress.rasanti.begodfree.framework.Entity;

public class ActionManager {
    
    private static List<Action> actions;
    static {
        actions = new ArrayList<Action>();
        actions.add(new MoveToAction());
        actions.add(new FeedAction());
    }
    
    public static Action findBestAction(Entity entity, BeGodGame game, Map<String,Object> goals, Object[] params) {
        List<Action> candidates = new ArrayList<Action>();
        for (Action a : actions) {
            boolean isCandidate = true;
            for (Map.Entry<String, Object> entry : goals.entrySet()) {
                if (!a.effects().containsKey(entry.getKey())
                        || !a.effects().get(entry.getKey()).equals(entry.getValue())) {
                    
                    isCandidate = false;
                    break;
                }
            }
            if (isCandidate) {
                candidates.add(a);
            }
        }
        
        if (!candidates.isEmpty()) {
            Collections.sort(candidates, new ActionComparator(entity, game, params));
            return candidates.get(0);
        }
        return null;
    }

}
