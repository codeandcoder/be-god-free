package com.wordpress.rasanti.begodfree.ai;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.begodfree.BeGodGame;
import com.wordpress.rasanti.begodfree.framework.Entity;
import com.wordpress.rasanti.begodfree.pathfinding.APathfinder;
import com.wordpress.rasanti.begodfree.utils.EntityUtils;

public class MoveToAction implements Action {

    @Override
    public boolean areConditionsFulfilled(Entity entity, BeGodGame game, Object[] params) {
        return true;
    }
    
    @Override
    public void init(Entity entity, BeGodGame game, int iteration, Object[] params) {
        entity.currentPath = APathfinder.findPath(game, entity, (Vector2) params[0]);
    }

    @Override
    public void execute(Entity entity, BeGodGame game, int iteration, Object[] params) {
        if (!isFinished(entity, game, iteration, params)) {
            Vector2 target = entity.currentPath.remove(0);
            if (EntityUtils.isObstacle(game.getWorld().getSquare(target))) {
                init(entity, game, iteration, params);
            } else {
                entity.move(target);
            }
        }
    }

    @Override
    public boolean isFinished(Entity entity, BeGodGame game, int iteration, Object[] params) {
        return entity.currentPath == null || entity.currentPath.isEmpty();
    }

    @Override
    public boolean isSuccessful(Entity entity, BeGodGame game, int iteration, Object[] params) {
        Vector2 target = (Vector2) params[0];
        return entity.getPosition().x == target.x && entity.getPosition().y == target.y;
    }

    @Override
    public Map<String, Object> conditions() {
        return null;
    }

    @Override
    public Map<String, Object> effects() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("moved", true);
        return map;
    }

    @Override
    public int calculateCost(Entity entity, BeGodGame game, Object[] params) {
        int xDifference = (int) Math.abs(((Vector2) params[0]).x - entity.getPosition().x);
        int yDifference = (int) Math.abs(((Vector2) params[0]).y - entity.getPosition().y);
        return (xDifference + yDifference);
    }

}
