package com.wordpress.rasanti.begodfree.ai;

import java.util.Map;

import com.wordpress.rasanti.begodfree.BeGodGame;
import com.wordpress.rasanti.begodfree.framework.Entity;

public interface Action {

    public abstract boolean areConditionsFulfilled(Entity entity, BeGodGame game, Object[] params);
    public abstract void init(Entity entity, BeGodGame game, int iteration, Object[] params);
    public abstract void execute(Entity entity, BeGodGame game, int iteration, Object[] params);
    public abstract boolean isFinished(Entity entity, BeGodGame game, int iteration, Object[] params);
    public abstract boolean isSuccessful(Entity entity, BeGodGame game, int iteration, Object[] params);
    public abstract Map<String,Object> conditions();
    public abstract Map<String,Object> effects();
    public abstract int calculateCost(Entity entity, BeGodGame game, Object[] params);
    
}
