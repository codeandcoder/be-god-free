package com.wordpress.rasanti.begodfree.ai;

public interface TaskListener {

    public abstract void onAllTasksFinished();
    
}
