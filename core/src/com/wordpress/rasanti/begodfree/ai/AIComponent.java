package com.wordpress.rasanti.begodfree.ai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.wordpress.rasanti.begodfree.BeGodGame;
import com.wordpress.rasanti.begodfree.framework.Entity;

public class AIComponent {
    
    private BeGodGame game;
    private Entity entity;
    private List<Task> tasks = new ArrayList<Task>();
    private Task activeTask;
    private TaskListener listener;
    
    public AIComponent(BeGodGame game, Entity entity) {
        this.game = game;
        this.entity = entity;
    }
    
    public void think(int iteration) {
        if (activeTask == null || activeTask.isFinished()) {
            if (!tasks.isEmpty()) {
                activeTask = tasks.remove(0);
            } else if (listener != null) {
                listener.onAllTasksFinished();
            }
        } else {
            activeTask.execute(entity, game, iteration);
        }
    }
    
    public void addTask(Task task) {
        tasks.add(task);
        Collections.sort(tasks);
    }
    
    public void setTaskListener(TaskListener listener) {
        this.listener = listener;
    }

}
