package com.wordpress.rasanti.begodfree.ai;

import java.util.Comparator;

import com.wordpress.rasanti.begodfree.framework.Entity;
import com.wordpress.rasanti.begodfree.BeGodGame;

public class ActionComparator implements Comparator<Action> {
    
    private Entity entity;
    private BeGodGame game;
    private Object[] params;
    
    public ActionComparator(Entity entity, BeGodGame game, Object[] params) {
        this.entity = entity;
        this.game = game;
        this.params = params;
    }

    @Override
    public int compare(Action o1, Action o2) {
        int cost1 = o1.calculateCost(entity, game, params);
        int cost2 = o2.calculateCost(entity, game, params);
        
        return cost2 - cost1;
    }

}
