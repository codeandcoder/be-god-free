package com.wordpress.rasanti.begodfree.ai;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.wordpress.rasanti.begodfree.BeGodGame;
import com.wordpress.rasanti.begodfree.framework.Entity;

public class Task implements Comparable<Task> {

    private String name;
    private List<Action> actions;
    private Action activeAction;
    private boolean finished = false;
    private boolean successful = false;
    private Object[] params;
    public int priority;
    
    
    public Task(String name, Entity entity, BeGodGame game, String goalKey, Object goalValue, int priority, Object... params) {
        this.name = name;
        this.priority = priority;
        this.params = params;
        buildActions(entity, game, goalKey, goalValue);
    }
    
    private void buildActions(Entity entity, BeGodGame game, String goalKey, Object goalValue) {
        actions = new ArrayList<Action>();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(goalKey, goalValue);
        
        Action currentAction = ActionManager.findBestAction(entity, game, map, params);
        actions.add(currentAction);
        while (currentAction.conditions() != null && !currentAction.areConditionsFulfilled(entity, game, params)) {
            currentAction = ActionManager.findBestAction(entity, game, currentAction.conditions(), params);
            actions.add(0, currentAction);
        }
    }
    
    public void execute(Entity entity, BeGodGame game, int iteration) {
        if (!finished) {
            if (activeAction == null || activeAction.isFinished(entity, game, iteration, params)) {
                if (activeAction != null && !activeAction.isSuccessful(entity, game, iteration, params)) {
                    successful = false;
                    finished = true;
                } else {
                    if (actions.isEmpty()) {
                        successful = true;
                        finished = true;
                    } else {
                        activeAction = actions.remove(0);
                        activeAction.init(entity, game, iteration, params);
                    }
                }
            } else {
                activeAction.execute(entity, game, iteration, params);
            }
        }
    }
    
    @Override
    public int compareTo(Task o) {
        return o.priority - priority;
    }
    
    public boolean isFinished() {
        return finished;
    }
    
    public boolean isSuccessful() {
        return successful;
    }
    
    public String getName() {
        return name;
    }
    
}
