package com.wordpress.rasanti.begodfree;

import com.badlogic.gdx.math.Vector2;

public interface WorldGenerator {

    public abstract void generateWorld(BeGodGame game, BeGodWorld world, Vector2 size);
    
}
