package com.wordpress.rasanti.begodfree.items;

public class TreeLog extends Item {

    private int lifeTime;
    
    public TreeLog(int lifeTime) {
        this.lifeTime = lifeTime;
    }
    
    public int getLifeTime() {
        return lifeTime;
    }
    
}
