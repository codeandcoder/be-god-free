package com.wordpress.rasanti.begodfree.utils;

import java.util.Random;

public class RandomUtils {
    
    private static Random random = new Random();

    public static Random getRandom() {
        return random;
    }
    
    public static int getIntBetween(int min, int max) {
        return random.nextInt(max - min) + min;
    }
    
}
