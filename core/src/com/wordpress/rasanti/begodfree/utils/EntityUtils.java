package com.wordpress.rasanti.begodfree.utils;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.begodfree.BeGodGame;
import com.wordpress.rasanti.begodfree.BeGodWorld;
import com.wordpress.rasanti.begodfree.entities.Feeder;
import com.wordpress.rasanti.begodfree.entities.FreshWater;
import com.wordpress.rasanti.begodfree.entities.FruitTree;
import com.wordpress.rasanti.begodfree.entities.Grass;
import com.wordpress.rasanti.begodfree.entities.Person;
import com.wordpress.rasanti.begodfree.entities.SaltWater;
import com.wordpress.rasanti.begodfree.entities.Tree;
import com.wordpress.rasanti.begodfree.entities.groups.Forest;
import com.wordpress.rasanti.begodfree.framework.Entity;
import com.wordpress.rasanti.begodfree.framework.EntityGroup;

public class EntityUtils {

    public static boolean isInMap(int maxX, int maxY, Vector2 pos) {
        return pos.x >= 0 && pos.y >= 0 && pos.x < maxX && pos.y < maxY;
    }
    
    public static boolean isWetFreshWater(BeGodWorld world, Vector2 pos) {
        int maxY = world.getSizeY();
        int maxX = world.getSizeX();
        
        if (!isInMap(maxX, maxY, pos))
            return false;
        
        return pos.y+1 < maxY && hasEntity(world.getSquare(new Vector2(pos.x,pos.y+1)), FreshWater.class)
                || pos.y+2 < maxY && hasEntity(world.getSquare(new Vector2(pos.x,pos.y+2)), FreshWater.class)
                || pos.x+1 < maxX && hasEntity(world.getSquare(new Vector2(pos.x+1,pos.y)), FreshWater.class)
                || pos.x+2 < maxX && hasEntity(world.getSquare(new Vector2(pos.x+2,pos.y)), FreshWater.class)
                || pos.y+1 < maxY && pos.x+1 < maxX && hasEntity(world.getSquare(new Vector2(pos.x+1,pos.y+1)), FreshWater.class)
                || pos.y-1 >= 0 && pos.x-1 >= 0 && hasEntity(world.getSquare(new Vector2(pos.x-1,pos.y-1)), FreshWater.class)
                || pos.y+1 < maxY && pos.x-1 >= 0 && hasEntity(world.getSquare(new Vector2(pos.x-1,pos.y+1)), FreshWater.class)
                || pos.y-1 >= 0 && pos.x+1 < maxX && hasEntity(world.getSquare(new Vector2(pos.x+1,pos.y-1)), FreshWater.class)
                || pos.y-1 >= 0 && hasEntity(world.getSquare(new Vector2(pos.x,pos.y-1)), FreshWater.class)
                || pos.y-2 >= 0 && hasEntity(world.getSquare(new Vector2(pos.x,pos.y-2)), FreshWater.class)
                || pos.x-1 >= 0 && hasEntity(world.getSquare(new Vector2(pos.x-1,pos.y)), FreshWater.class)
                || pos.x-2 >= 0 && hasEntity(world.getSquare(new Vector2(pos.x-2,pos.y)), FreshWater.class);
    }
    
    public static boolean hasEntity(List<Entity> entities, Class<?> c) {
        for (Entity e : entities) {
            if (e.getClass().equals(c)) {
                return true;
            }
        }
        
        return false;
    }
    
    public static List<Vector2> getWetSquares(BeGodWorld world) {
        List<Vector2> wetPlaces = new ArrayList<Vector2>();
        
        for (int y = 0; y < world.getSizeY(); y++) {
            for (int x = 0; x < world.getSizeX(); x++) {
                Vector2 pos = new Vector2(x,y);
                if (isWetFreshWater(world, pos)) {
                    wetPlaces.add(pos);
                }
            }
        }
        
        return wetPlaces;
    }
    
    public static Feeder findNearestFood(Entity entity, BeGodGame game) {
        float bestDistance = 100000;
        Feeder closest = null;
        List<Feeder> candidates = findFoodCandidates(entity, game);
        for (Feeder f : candidates) {
            if (!f.isExhausted()) {
                Vector2 p1 = entity.getPosition();
                Vector2 p2 = ((Entity)f).getPosition();
                float currentDistance = Vector2.dst(p1.x, p1.y, p2.x, p2.y);
                if (closest == null || currentDistance < bestDistance) {
                    closest = f;
                    bestDistance = currentDistance;
                }
            }
        }
        return closest;
    }
    
    private static List<Feeder> findFoodCandidates(Entity entity, BeGodGame game) {
        List<Feeder> feeders = new ArrayList<Feeder>();
        
        List<EntityGroup<?>> groups = game.getWorld().getGroups(Forest.class);
        for (EntityGroup<?> group : groups) {
            Forest f = (Forest) group;
            List<Tree> trees = f.getEntities();
            for (Tree t : trees) {
                if (t instanceof Feeder) {
                    feeders.add((Feeder) t);
                }
            }
        }
        
        return feeders;
    }
    
    public static Vector2 fromNumToCoordinates(int num) {
        Vector2 result = new Vector2(0,0);
        
        switch(num) {
        case 0:
            result.x = -1;
            result.y =  1;
            break;
        case 1:
            result.x =  0;
            result.y =  1;
            break;
        case 2:
            result.x = +1;
            result.y =  1;
            break;
        case 3:
            result.x = -1;
            result.y =  0;
            break;
        case 4:
            result.x = +1;
            result.y =  0;
            break;
        case 5:
            result.x = -1;
            result.y = -1;
            break;
        case 6:
            result.x =  0;
            result.y = -1;
            break;
        case 7:
            result.x =  1;
            result.y = -1;
            break;
        }
        
        return result;
    }
    
    public static boolean isSuitableToPlant(List<Entity> square) {
        return hasEntity(square, Grass.class) && !hasEntity(square, Tree.class)
                && !hasEntity(square, FruitTree.class) && !hasEntity(square, Person.class);
    }
    
    public static boolean isObstacle(List<Entity> square) {
        return hasEntity(square, Tree.class)
        || hasEntity(square, FreshWater.class)
        || hasEntity(square, SaltWater.class)
        || hasEntity(square, FruitTree.class);
    }
    
}
