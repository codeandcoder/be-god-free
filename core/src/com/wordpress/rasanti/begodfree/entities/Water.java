package com.wordpress.rasanti.begodfree.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.begodfree.BeGodGame;
import com.wordpress.rasanti.begodfree.framework.Entity;

public abstract class Water extends Entity {

    public Water(BeGodGame game, String name, Vector2 position) {
        super(game, name, position);
    }

    @Override
    public void init() {
        aspect = new Sprite(game.getResources().getTexture("water"));
    }

}
