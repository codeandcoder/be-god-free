package com.wordpress.rasanti.begodfree.entities;


public interface Feeder {

    public abstract void consume(Eater eater, int amount);
    public abstract boolean isExhausted();
    
}
