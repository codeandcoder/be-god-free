package com.wordpress.rasanti.begodfree.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.begodfree.BeGodGame;
import com.wordpress.rasanti.begodfree.entities.groups.Forest;

public class FruitTree extends Tree implements Feeder {
    
    private static final int MAX_FOOD = 10;
    private static final int FOOD_RATE = 100;
    private int food = 0;

    public FruitTree(BeGodGame game, String name, Vector2 position, Forest group) {
        super(game, name, position, group);
    }

    @Override
    public void init() {
        super.init();
    }
    
    @Override
    public void update(int iteration) {
        super.update(iteration);
        
        // Each 10 iterations, grow food
        if (growingState == 2 && lifeTime % FOOD_RATE == 0 && food < MAX_FOOD) {
            if (food == 0) {
                aspect = new Sprite(game.getResources().getTexture("fruit-tree"));
            }
            food++;
        }
    }
    
    @Override
    public void consume(Eater eater, int amount) {
        food -= amount;
        food = food < 0 ? 0 : food;
        if (food == 0) {
            aspect = new Sprite(game.getResources().getTexture("tree"));
        }
    }

    @Override
    public boolean isExhausted() {
        return food == 0;
    }

}
