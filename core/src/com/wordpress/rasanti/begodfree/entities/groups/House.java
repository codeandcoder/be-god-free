package com.wordpress.rasanti.begodfree.entities.groups;

import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.begodfree.BeGodGame;
import com.wordpress.rasanti.begodfree.entities.HouseFloor;
import com.wordpress.rasanti.begodfree.framework.Entity;
import com.wordpress.rasanti.begodfree.framework.EntityGroup;

public class House extends EntityGroup<HouseFloor> {

    private boolean built = false;
    
    public House(BeGodGame game, String name, Vector2 position) {
        super(game, name, position);
    }

    @Override
    public void destroy() {
        
    }
    
    public static House findAvailableHouse(BeGodGame game, Entity entity) {
        // Provisional, we should find a good place.
        House house = new House(game, entity.getName() + "'s house", new Vector2(8,8));
        return house;
    }
    
    // Getters && Setters
    public boolean isBuilt() {
        return built;
    }

}
