package com.wordpress.rasanti.begodfree.entities.groups;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.begodfree.BeGodGame;
import com.wordpress.rasanti.begodfree.entities.FreshWater;
import com.wordpress.rasanti.begodfree.framework.Entity;
import com.wordpress.rasanti.begodfree.framework.EntityGroup;

public class Lake extends EntityGroup<FreshWater> {
    
    private Random random;
    private List<Integer> randomsUsed;

    public Lake(BeGodGame game, String name, Vector2 position) {
        super(game, name, position);
    }

    @Override
    public void init() {
        super.init();
        random = game.getWorld().getRandom();
        randomsUsed = new ArrayList<Integer>();
        addEntity((FreshWater) new FreshWater(game, "Water", new Vector2(position.x,position.y)).setDrawPriority(1));
        addEntity((FreshWater) new FreshWater(game, "Water", new Vector2(position.x+1,position.y)).setDrawPriority(1));
        addEntity((FreshWater) new FreshWater(game, "Water", new Vector2(position.x,position.y+1)).setDrawPriority(1));
        addEntity((FreshWater) new FreshWater(game, "Water", new Vector2(position.x,position.y-1)).setDrawPriority(1));
        addEntity((FreshWater) new FreshWater(game, "Water", new Vector2(position.x-1,position.y)).setDrawPriority(1));
        for (int i = 0; i < 6; i++) {
            putRandomWater(Math.abs(random.nextInt() % 8));
        }
    }

    @Override
    public void destroy() {
    }
    
    private void putRandomWater(int rand) {
        if (randomsUsed.contains(rand)) {
            putRandomWater((rand+1) % 8);
        } else {
            randomsUsed.add(rand);
            switch(rand) {
            case 0:
                addEntity((FreshWater) new FreshWater(game, "Water", new Vector2(position.x+2,position.y)).setDrawPriority(1));
                break;
            case 1:
                addEntity((FreshWater) new FreshWater(game, "Water", new Vector2(position.x+1,position.y+1)).setDrawPriority(1));
                break;
            case 2:
                addEntity((FreshWater) new FreshWater(game, "Water", new Vector2(position.x+1,position.y-1)).setDrawPriority(1));
                break;
            case 3:
                addEntity((FreshWater) new FreshWater(game, "Water", new Vector2(position.x,position.y+2)).setDrawPriority(1));
                break;
            case 4:
                addEntity((FreshWater) new FreshWater(game, "Water", new Vector2(position.x,position.y-2)).setDrawPriority(1));
                break;
            case 5:
                addEntity((FreshWater) new FreshWater(game, "Water", new Vector2(position.x-1,position.y+1)).setDrawPriority(1));
                break;
            case 6:
                addEntity((FreshWater) new FreshWater(game, "Water", new Vector2(position.x-1,position.y-1)).setDrawPriority(1));
                break;
            case 7:
                addEntity((FreshWater) new FreshWater(game, "Water", new Vector2(position.x-2,position.y)).setDrawPriority(1));
                break;
            }
        }
    }

}
