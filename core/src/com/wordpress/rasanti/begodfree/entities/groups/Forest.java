package com.wordpress.rasanti.begodfree.entities.groups;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.begodfree.BeGodGame;
import com.wordpress.rasanti.begodfree.entities.FruitTree;
import com.wordpress.rasanti.begodfree.entities.Grass;
import com.wordpress.rasanti.begodfree.entities.Tree;
import com.wordpress.rasanti.begodfree.framework.EntityGroup;
import com.wordpress.rasanti.begodfree.utils.EntityUtils;

public class Forest extends EntityGroup<Tree> {

    private Random random;
    private List<Integer> randomsUsed;
    
    public Forest(BeGodGame game, String name, Vector2 position) {
        super(game, name, position);
    }
    
    @Override
    public void init() {
        super.init();
        random = game.getWorld().getRandom();
        randomsUsed = new ArrayList<Integer>();
        addEntity((Tree) new Tree(game, "Tree", new Vector2(position.x,position.y), this).setDrawPriority(1));
        addEntity((Tree) new Tree(game, "Tree", new Vector2(position.x+1,position.y), this).setDrawPriority(1));
        addEntity((Tree) new Tree(game, "Tree", new Vector2(position.x,position.y+1), this).setDrawPriority(1));
        addEntity((Tree) new Tree(game, "Tree", new Vector2(position.x,position.y-1), this).setDrawPriority(1));
        addEntity((Tree) new Tree(game, "Tree", new Vector2(position.x-1,position.y), this).setDrawPriority(1));
        for (int i = 0; i < 6; i++) {
            putRandomTree(Math.abs(random.nextInt() % 8), Math.abs(random.nextInt() % 2));
        }
    }
    
    private void putRandomTree(int rand, int treeType) {
        if (randomsUsed.contains(rand)) {
            putRandomTree((rand+1) % 8, treeType);
        } else {
            randomsUsed.add(rand);
            Vector2 pos = null;
            switch(rand) {
            case 0:
                pos = new Vector2(position.x+2,position.y);
                break;
            case 1:
                pos = new Vector2(position.x+1,position.y+1);
                break;
            case 2:
                pos = new Vector2(position.x+1,position.y-1);
                break;
            case 3:
                pos = new Vector2(position.x,position.y+2);
                break;
            case 4:
                pos = new Vector2(position.x,position.y-2);
                break;
            case 5:
                pos = new Vector2(position.x-1,position.y+1);
                break;
            case 6:
                pos = new Vector2(position.x-1,position.y-1);
                break;
            case 7:
                pos = new Vector2(position.x-2,position.y);
                break;
            }
            
            if (treeType == 0) {
                addEntity((Tree) new Tree(game, "Tree", pos, this).setDrawPriority(1));
            } else {
                addEntity((Tree) new FruitTree(game, "FruitTree", pos, this).setDrawPriority(1));
            }
        }
    }
    
    @Override
    public void addEntity(Tree entity) {
        if (EntityUtils.isInMap(game.getWorld().getSizeX(), game.getWorld().getSizeY(), entity.getPosition())
                && EntityUtils.hasEntity(game.getWorld().getSquare(entity.getPosition()), Grass.class)
                && EntityUtils.isWetFreshWater(game.getWorld(), entity.getPosition())
                && entities.add(entity)) {
            entity.setGroup(this);
        }
    }

    @Override
    public void destroy() {
    }

}
