package com.wordpress.rasanti.begodfree.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.begodfree.BeGodGame;
import com.wordpress.rasanti.begodfree.entities.groups.Forest;
import com.wordpress.rasanti.begodfree.framework.Entity;
import com.wordpress.rasanti.begodfree.utils.EntityUtils;
import com.wordpress.rasanti.begodfree.utils.RandomUtils;

public class Tree extends Entity {

    private static final int MIN_REPR_RATE = 26;
    private static final int MAX_REPR_RATE = 40;
    private static final int MIN_GROW_RATE = 15;
    private static final int MAX_GROW_RATE = 25;
    private static final int MIN_RESISTANCE = 1;
    private static final int MAX_RESISTANCE = 3;
    
    protected Forest group;
    protected int reproductionRate;
    protected int growingRate;
    protected int growingState = 0;
    protected int offsprings = 1;
    protected int resistance;
    
    public Tree(BeGodGame game, String name, Vector2 position, Forest group) {
        super(game, name, position);
        this.group = group;
        reproductionRate = RandomUtils.getIntBetween(MIN_REPR_RATE, MAX_REPR_RATE);
        growingRate = RandomUtils.getIntBetween(MIN_GROW_RATE, MAX_GROW_RATE);
        resistance = RandomUtils.getIntBetween(MIN_RESISTANCE, MAX_RESISTANCE);
    }

    @Override
    public void init() {
        aspect = new Sprite(game.getResources().getTexture("tree"));
        aspect.scale(-0.5f);
    }
    
    @Override
    public void update(int iteration) {
        super.update(iteration);
        
        if (lifeTime % growingRate == 0 && growingState < 2) {
            grow();
        }
        
        if (lifeTime % reproductionRate == 0 && growingState == 2) {
            reproduce();
        }
    }

    @Override
    public void destroy() {
        super.destroy();
    }
    
    public void grow() {
        resistance++;
        growingState++;
        aspect.scale(0.25f);
    }
    
    public void reproduce() {
        for (int i = 0; i < offsprings; i++) {
            int num = RandomUtils.getRandom().nextInt(8);
            Vector2 coord = EntityUtils.fromNumToCoordinates(num);
            coord.x += position.x;
            coord.y += position.y;
            int tryCounter = 0;
            while (tryCounter < 8) {
                tryCounter++;
                if (EntityUtils.isSuitableToPlant(game.getWorld().getSquare(coord))) {
                    int treeType = RandomUtils.getRandom().nextInt(2);
                    if (treeType == 0) {
                        Tree newTree = (Tree) new Tree(game, "Tree", coord, group).setDrawPriority(1);
                        group.addEntity(newTree);
                        game.getWorld().getSquare(coord).add(newTree);
                    } else {
                        Tree newTree = (Tree) new FruitTree(game, "FruitTree", coord, group).setDrawPriority(1);
                        group.addEntity(newTree);
                        game.getWorld().getSquare(coord).add(newTree);
                    }
                    break;
                } else {
                    num = (num + 1) % 8;
                    coord = EntityUtils.fromNumToCoordinates(num);
                    coord.x += position.x;
                    coord.y += position.y;
                }
            }
        }
    }

}
