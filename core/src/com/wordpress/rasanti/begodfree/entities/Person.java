package com.wordpress.rasanti.begodfree.entities;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.begodfree.BeGodGame;
import com.wordpress.rasanti.begodfree.ai.Task;
import com.wordpress.rasanti.begodfree.entities.groups.House;
import com.wordpress.rasanti.begodfree.framework.Entity;
import com.wordpress.rasanti.begodfree.items.Item;
import com.wordpress.rasanti.begodfree.utils.EntityUtils;
import com.wordpress.rasanti.begodfree.utils.RandomUtils;

public class Person extends Entity implements Eater {
    
    private int hunger = 0;
    private boolean feeding = false;
    private House house;
    
    private List<Item> inventory = new ArrayList<Item>();
    
    public enum DeathCause { Hunger };
    
    public Person(BeGodGame game, String name, Vector2 position) {
        super(game, name, position);
    }

    @Override
    public void init() {
        aspect = new Sprite(game.getResources().getTexture("person"));
    }
    
    @Override
    public void update(int iteration) {
        super.update(iteration);
        
        // Each 5 iterations, add hunger
        if (lifeTime % 5 == 0) {
            hunger++;
        }
        
        // Probabilities of going to eat
        if (!feeding && lifeTime % 5 == 0) {
            int random = RandomUtils.getRandom().nextInt(100);
            if (random <= hunger) {
                Feeder feeder = EntityUtils.findNearestFood(this, game);
                if (feeder != null) {
                    ai.addTask(new Task("Feeding", this, game, "feed", true, 11, ((Entity)feeder).getPosition(), feeder));
                    feeding = true;
                }
            }
        }
        
        // Find house
        if (house == null) {
            house = House.findAvailableHouse(game, this);
            // Build house
            if (!house.isBuilt()) {
                
            }
        }
        
        if (hunger == 100) {
            die(DeathCause.Hunger);
        }
    }

    @Override
    public void destroy() {
        super.destroy();
    }
    
    @Override
    public void onAllTasksFinished() {
        ai.addTask(new Task("Moving", this, game, "moved", true, 10, new Vector2(35,35)));
        ai.addTask(new Task("Moving", this, game, "moved", true, 9, new Vector2(37,16)));
        ai.addTask(new Task("Moving", this, game, "moved", true, 8, new Vector2(5,37)));
    }

    @Override
    public void eat(Feeder feeder, int amount) {
        hunger -= amount;
        hunger = hunger < 0 ? 0 : hunger;
        if (hunger == 0) {
            feeding = false;
        }
    }

    @Override
    public boolean isSatisfied() {
        return hunger == 0;
    }
    
    public void die(DeathCause cause) {
        destroy();
    }
    
    public int getAmountOfItem(Class<? extends Item> c) {
        int result = 0;
        
        for(Item i : inventory) {
            if (i.getClass().equals(c)) {
                result++;
            }
        }
            
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public <T extends Item> T getItemOfType(Class<T> c) {
        for(Item i : inventory) {
            if (c.isInstance(i)) {
                return (T) i;
            }
        }
            
        return null;
    }
    
    public void addItem(Item i) {
        inventory.add(i);
    }
    
    public void removeItem(Item i) {
        
    }

}
