package com.wordpress.rasanti.begodfree.entities;

import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.begodfree.BeGodGame;

public class SaltWater extends Water {

    public SaltWater(BeGodGame game, String name, Vector2 position) {
        super(game, name, position);
    }

    @Override
    public void destroy() {
        super.destroy();
    }

}
