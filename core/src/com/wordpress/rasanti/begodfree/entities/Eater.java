package com.wordpress.rasanti.begodfree.entities;


public interface Eater {

    public abstract void eat(Feeder feeder, int amount);
    public abstract boolean isSatisfied();
    
}
