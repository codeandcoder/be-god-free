package com.wordpress.rasanti.begodfree.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.begodfree.BeGodGame;
import com.wordpress.rasanti.begodfree.framework.Entity;

public class Grass extends Entity {

    public Grass(BeGodGame game, String name, Vector2 position) {
        super(game, name, position);
    }

    @Override
    public void init() {
        aspect = new Sprite(game.getResources().getTexture("grass"));
    }

    @Override
    public void update(int iteration) {
        super.update(iteration);
    }

    @Override
    public void destroy() {
        super.destroy();
    }

}
