package com.wordpress.rasanti.begodfree;

public class CommandInterpreter {

    public static String lastCommand = "command";
    
    public static void execute(BeGodGame game, String command) {
        if (command.contains("speed")) {
            String value = command.split(" ")[1].trim();
            float vel = Float.valueOf(value);
            BeGodGame.ITERATION_LENGTH = vel;
        } else if (command.contains("size")) {
            String value = command.split(" ")[1].trim();
            float size = Float.valueOf(value);
            game.getWorld().setSquareSize(size);
        } else if (command.contains("camsp")) {
            String value = command.split(" ")[1].trim();
            int vel = Integer.valueOf(value);
            BeGodGame.CAMERA_SPEED = vel;
        } else if (command.contains("restart")) {
            String[] params = command.split(" ");
            String seed = params.length > 1 ? params[1] : "1000000";
            int sizeX = params.length > 2 ? Integer.parseInt(params[2]) : 40;
            int sizeY = params.length > 3 ? Integer.parseInt(params[3]) : 40;
            game.restart(seed, sizeX, sizeY);
        }
        
        lastCommand = command;
    }
    
}
