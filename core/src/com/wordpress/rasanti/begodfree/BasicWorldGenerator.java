package com.wordpress.rasanti.begodfree;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.begodfree.entities.Grass;
import com.wordpress.rasanti.begodfree.entities.Person;
import com.wordpress.rasanti.begodfree.entities.SaltWater;
import com.wordpress.rasanti.begodfree.entities.groups.Forest;
import com.wordpress.rasanti.begodfree.entities.groups.Lake;
import com.wordpress.rasanti.begodfree.framework.Entity;
import com.wordpress.rasanti.begodfree.utils.EntityUtils;

/**
 * World generation algorithm.
 * 
 * @author Santi Ruiz
 */
public class BasicWorldGenerator implements WorldGenerator {
    
    private static BasicWorldGenerator singleton;
    
    private BasicWorldGenerator() {
    }
    
    public static BasicWorldGenerator getInstance() {
        if (singleton == null) {
            singleton = new BasicWorldGenerator();
        }
        
        return singleton;
    }

    public void generateWorld(BeGodGame game, BeGodWorld world, Vector2 size) {
        String seed = world.getSeed();
        Random random = world.getRandom();
        world.setSquares(generateBase(game, size));
        
        // Lakes generation
        List<Vector2> lakePositions = new ArrayList<Vector2>();
        int numberOfLakes = (int)size.x / 5;
        for (int i = 0; i < numberOfLakes; i++) {
            int randomX = Math.abs(random.nextInt() % (int)size.x);
            int randomY = Math.abs(random.nextInt() % (int)size.y);
            Vector2 lakePos = new Vector2(randomX, randomY);
            if (lakePositions.contains(lakePos)) {
                i--;
            } else {
                lakePositions.add(lakePos);
                
                Lake lake = new Lake(game, "Lake", lakePos);
                lake.setAllEntitiesToSquares(world);
            }
        }
        
        // Forests generation
        List<Integer> randomPlaces = new ArrayList<Integer>();
        List<Vector2> wetPlaces = EntityUtils.getWetSquares(world);
        int numberOfForests = (int)size.x / 3;
        for (int i = 0; i < numberOfForests; i++) {
            int randomPlace = Math.abs(random.nextInt() % wetPlaces.size());
            
            if (randomPlaces.contains(randomPlace)) {
                i--;
            } else {
                randomPlaces.add(randomPlace);
                
                Forest forest = new Forest(game, "Forest", wetPlaces.get(randomPlace));
                forest.addAllEntitiesToSquares(world);
            }
        }
        
        // Person generator
        Person person = new Person(game, "Person", new Vector2(5,5));
        person.setDrawPriority(2);
        world.getSquare(5,5).add(person);
        
        
        Gdx.app.debug("WorldGeneration", "World generated, seed: " + seed);
    }
    
    private ArrayList<ArrayList<ArrayList<Entity>>> generateBase(BeGodGame game, Vector2 size) {
        ArrayList<ArrayList<ArrayList<Entity>>> squares = new ArrayList<ArrayList<ArrayList<Entity>>>();
        
        for (int y = 0; y < size.y; y++) {
            squares.add(new ArrayList<ArrayList<Entity>>());
            for (int x = 0; x < size.x; x++) {
                squares.get(y).add(new ArrayList<Entity>());
                if (x == 0 || y == 0 || x == size.x-1 || y == size.y-1) {
                    squares.get(y).get(x).add(new SaltWater(game, "Water", new Vector2(x,y)));
                } else {
                    squares.get(y).get(x).add(new Grass(game, "Grass", new Vector2(x,y)));
                }
            }
        }
        
        return squares;
    }
    
}
