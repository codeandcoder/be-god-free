package com.wordpress.rasanti.begodfree;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.wordpress.rasanti.begodfree.framework.GUIScreen;
import com.wordpress.rasanti.begodfree.framework.Game;
import com.wordpress.rasanti.begodfree.framework.InternalFilesLoader;
import com.wordpress.rasanti.begodfree.framework.ResourceLoader;
import com.wordpress.rasanti.begodfree.gui.GameScreen;

/**
 * Be God main class.
 * 
 * @author Santi Ruiz
 */
public class BeGodGame extends Game {
    
    // Resource paths
    public static final String TEXTURES_PATH = "textures";
    public static final String SOUND_PATH = "sound";
    public static final String MUSIC_PATH = "music";
    public static final String I18N_PATH = "I18N";
    
    public static final float INITIAL_SQUARE_SIZE = 11;
    public static float CAMERA_SPEED = 500;
    public static float ITERATION_LENGTH = 0.01f;
    private BeGodWorld world;
    private long nextEntityID = 0;
    
    private List<GUIScreen> screens = new ArrayList<GUIScreen>();

    @Override
    protected void init() {
        world = new BeGodWorld(this, INITIAL_SQUARE_SIZE);
        world.init(40, 40, "1000000");
        screens.add(GUIScreen.buildScreen(this, GameScreen.class));
    }
    
    public void restart(String seed, int sizeX, int sizeY) {
        world = new BeGodWorld(this, INITIAL_SQUARE_SIZE);
        world.init(sizeX, sizeY, seed);
    }

    @Override
    protected void update(float deltaTime) {
        if (world != null && world.isInitialized()) {
            world.update(deltaTime);
        }
        
        handleDebugInput(deltaTime);
    }

    @Override
    protected void render(SpriteBatch batch) {
        if (world != null && world.isInitialized()) {
            world.render(batch);
        }
    }

    @Override
    protected void renderGUI(SpriteBatch batch) {
        for (int i = 0; i < screens.size(); i++) {
            screens.get(i).render(batch);
        }
    }
    
    @Override
    protected void destroy() {
        if (world != null && world.isInitialized()) {
            world.destroy();
            world = null;
        }
    }

    @Override
    protected String getSplashScreenPath() {
        return "textures/splash/splash1.png";
    }

    @Override
    protected ResourceLoader getResourceLoader() {
        return new InternalFilesLoader(TEXTURES_PATH, SOUND_PATH, MUSIC_PATH, I18N_PATH);
    }
    
    private void handleDebugInput(float deltaTime) {
        if (Gdx.app.getType() != ApplicationType.Desktop)
            return;

        // Camera Controls (move)
        float camMoveSpeed = CAMERA_SPEED * deltaTime;
        if (Gdx.input.isKeyPressed(Keys.LEFT))
            camera.translate(-camMoveSpeed, 0);
        if (Gdx.input.isKeyPressed(Keys.RIGHT))
            camera.translate(camMoveSpeed, 0);
        if (Gdx.input.isKeyPressed(Keys.UP))
            camera.translate(0, camMoveSpeed);
        if (Gdx.input.isKeyPressed(Keys.DOWN))
            camera.translate(0, -camMoveSpeed);
            
    }
    
    // Getters && Setters
    
    public BeGodWorld getWorld() {
        return world;
    }
    
    public long getNextID() {
        return nextEntityID++;
    }
    
    
}
