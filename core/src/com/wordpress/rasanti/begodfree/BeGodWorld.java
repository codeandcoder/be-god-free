package com.wordpress.rasanti.begodfree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.begodfree.framework.Entity;
import com.wordpress.rasanti.begodfree.framework.Entity.DrawPriorityComparator;
import com.wordpress.rasanti.begodfree.framework.EntityGroup;

public class BeGodWorld {
    
    public static final int SEED_LENGTH = 7;
    
    private BeGodGame game;
    private boolean initialized = false;
    
    // World squares. Each square may contain multiple entities
    private ArrayList<ArrayList<ArrayList<Entity>>> squares = new ArrayList<ArrayList<ArrayList<Entity>>>();
    private Map<Class<?>, List<EntityGroup<?>>> entityGroups = new HashMap<Class<?>, List<EntityGroup<?>>>();
    private float squareSize;
    
    private String seed = "0000000";
    private Random random;
    
    private boolean drawPriorityChanged = true;
    private Vector2 whereDrawPriorityChanged = null;
    private DrawPriorityComparator drawPriorityComparator = new DrawPriorityComparator();
    
    private int currentIteration = 0;
    private long lastIteration;

    public BeGodWorld(BeGodGame game, float squareSize) {
        this.game = game;
        this.squareSize = squareSize;
    }
    
    public void init(int sizeX, int sizeY, String seed) {
        this.seed = formatSeed(seed);
        this.random = new Random(Long.parseLong(seed));
        BasicWorldGenerator.getInstance().generateWorld(game, this, new Vector2(sizeX, sizeY));
        entityGroups = scanGroups();
        initialized = true;
    }
    
    public void update(float deltaTime) {
        if (drawPriorityChanged) {
            drawPriorityChanged = false;
            recalculateDrawPriorities();
        }
        
        if (System.currentTimeMillis() - lastIteration >= BeGodGame.ITERATION_LENGTH * 1000) {
            currentIteration++;
            lastIteration = System.currentTimeMillis();
            updateEntities(currentIteration);
            resetEntitiesUpdateState();
        }
    }
    
    public void render(SpriteBatch batch) {
        int sizeY = squares.size();
        int sizeX = squares.get(0).size();
        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                ArrayList<Entity> entities = squares.get(y).get(x);
                for (int i = 0; i < entities.size(); i++) {
                    entities.get(i).render(batch);
                }
            }
        }
    }
    
    public void destroy() {
        int sizeY = squares.size();
        int sizeX = squares.get(0).size();
        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                ArrayList<Entity> entities = squares.get(y).get(x);
                for (int i = 0; i < entities.size(); i++) {
                    entities.get(i).destroy();
                }
                entities.clear();
            }
            squares.get(y).clear();
        }
        squares.clear();
    }
    
    private String formatSeed(String seed) {
        if (seed.length() > SEED_LENGTH) {
            return seed.substring(0,SEED_LENGTH);
        } else if (seed.length() < SEED_LENGTH) {
            return formatSeed(seed + "0");
        }
        
        return seed;
    }
    
    private void recalculateDrawPriorities() {
        // If the position is null, recalculate the whole world
        if (whereDrawPriorityChanged == null) {
            int sizeY = squares.size();
            int sizeX = squares.get(0).size();
            for (int y = 0; y < sizeY; y++) {
                for (int x = 0; x < sizeX; x++) {
                    Collections.sort(squares.get(y).get(x), drawPriorityComparator);
                }
            }
        } else {
            int x = (int) whereDrawPriorityChanged.x;
            int y = (int) whereDrawPriorityChanged.y;
            Collections.sort(squares.get(y).get(x), drawPriorityComparator);
        }
    }
    
    private void updateEntities(int iteration) {
        int sizeY = squares.size();
        int sizeX = squares.get(0).size();
        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                ArrayList<Entity> entities = squares.get(y).get(x);
                for (int i = 0; i < entities.size(); i++) {
                    Entity e = entities.get(i);
                    if (!e.isUpdated()) {
                        e.update(iteration);
                    }
                }
            }
        }
    }
    
    private void resetEntitiesUpdateState() {
        int sizeY = squares.size();
        int sizeX = squares.get(0).size();
        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                ArrayList<Entity> entities = squares.get(y).get(x);
                for (int i = 0; i < entities.size(); i++) {
                    Entity e = entities.get(i);
                    e.setUpdated(false);
                }
            }
        }
    }
    
    private Map<Class<?>, List<EntityGroup<?>>> scanGroups() {
        Map<Class<?>, List<EntityGroup<?>>> groups = new HashMap<Class<?>, List<EntityGroup<?>>>();
        int sizeY = squares.size();
        int sizeX = squares.get(0).size();
        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                ArrayList<Entity> entities = squares.get(y).get(x);
                for (int i = 0; i < entities.size(); i++) {
                    Entity e = entities.get(i);
                    if (e.getGroup() != null) {
                        if (groups.containsKey(e.getGroup().getClass())) {
                            List<EntityGroup<?>> groupsOfClass = groups.get(e.getGroup().getClass());
                            groupsOfClass.add(e.getGroup());
                        } else {
                            List<EntityGroup<?>> groupsOfClass = new ArrayList<EntityGroup<?>>();
                            groupsOfClass.add(e.getGroup());
                            groups.put(e.getGroup().getClass(), groupsOfClass);
                        }
                    }
                }
            }
        }
        return groups;
    }
    
    // Getters && Setters
    public boolean isInitialized() {
        return initialized;
    }
    
    public void setSquares(ArrayList<ArrayList<ArrayList<Entity>>> squares) {
        this.squares = squares;
    }

    public float getSquareSize() {
        return squareSize;
    }

    public void setSquareSize(float squareSize) {
        this.squareSize = squareSize;
    }

    public String getSeed() {
        return seed;
    }
    
    public int getSizeX() {
        return squares.get(0).size();
    }
    
    public int getSizeY() {
        return squares.size();
    }
    
    public List<Entity> getSquare(Vector2 position) {
        return squares.get((int)position.y).get((int)position.x);
    }
    
    public List<Entity> getSquare(int x, int y) {
        return squares.get(y).get(x);
    }
    
    public void drawPriorityChanged(Vector2 position) {
        whereDrawPriorityChanged = position;
        drawPriorityChanged = true;
    }
    
    public List<EntityGroup<?>> getGroups(Class<? extends EntityGroup<?>> c) {
        return entityGroups.get(c);
    }
    
    public EntityGroup<?> getGroup(Class<? extends EntityGroup<?>> c, String name) {
        List<EntityGroup<?>> list = getGroups(c);
        
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getName().equals(name)) {
                    return list.get(i);
                }
            }
        }
        
        return null;
    }
    
    public Random getRandom() {
        return random;
    }
    
}
