package com.wordpress.rasanti.begodfree.framework;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * This manages the whole live cycle of the game and the game loop.
 * Also, this is the starting point of every execution.
 * The main class of your game should inherits from this class.
 * 
 * @author Santi Ruiz
 */
public abstract class Game extends ApplicationAdapter {

    protected boolean initialized = false;
    private boolean showingSplashScreen = false;
    private Texture splashScreen;
    
    protected Color clearColor;
    protected SpriteBatch batch;
    protected OrthographicCamera camera;
    
    protected ResourceLoader resources;
    
    /*
     * Called at the very beginning.
     * 
     * @see com.badlogic.gdx.ApplicationAdapter#create()
     */
    @Override
    public final void create () {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);
        clearColor = new Color(0,0,0,1);
        batch = new SpriteBatch();
        resources = getResourceLoader();
        splashScreen = resources.getNoLoadedTexture(getSplashScreenPath());
        
        // Camera initialization
        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();
        camera = new OrthographicCamera(w, h);
        camera.position.set(camera.viewportWidth / 2f, camera.viewportHeight / 2f, 0);
        camera.update();
    }

    /*
     * Called every frame. 
     * It manages the splash screen showing and the calling to Init() method.
     * 
     * @see com.badlogic.gdx.ApplicationAdapter#render()
     */
    @Override
    public final void render () {
        camera.update();
        
        if (showingSplashScreen) {
            showingSplashScreen = false;
            resources.initResources();
            init();
            initialized = true;
        }
        
        if (initialized)
            update(Gdx.graphics.getDeltaTime());
        
        batch.setProjectionMatrix(camera.combined);
        
        Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        if (!initialized) {
            drawSplashScreen(batch);
            showingSplashScreen = true;
        } else {
            render(batch);
        }
        batch.end();
        
        if (initialized)
            renderGUI(batch);
    }
    
    @Override
    public void resize(int width, int height) {
        camera.viewportWidth = width;
        camera.viewportHeight = height;
        camera.update();
    }
    
    @Override
    public final void dispose() {
        super.dispose();
        
        resources.dispose();
        destroy();
    }
    
    private void drawSplashScreen(SpriteBatch batch) {
        if (splashScreen != null) {
            batch.draw(splashScreen,0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        }
    }
    
    protected abstract void init();
    protected abstract void update(float deltaTime);
    protected abstract void render(SpriteBatch batch);
    protected abstract void renderGUI(SpriteBatch batch);
    protected abstract void destroy();
    protected abstract String getSplashScreenPath();
    protected abstract ResourceLoader getResourceLoader();
    
    // Getters && Setters
    
    public boolean isInitialized() {
        return initialized;
    }

    public ResourceLoader getResources() {
        return resources;
    }
    
}
