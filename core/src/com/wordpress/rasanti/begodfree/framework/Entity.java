package com.wordpress.rasanti.begodfree.framework;

import java.util.Comparator;
import java.util.List;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.begodfree.BeGodGame;
import com.wordpress.rasanti.begodfree.ai.AIComponent;
import com.wordpress.rasanti.begodfree.ai.TaskListener;

public abstract class Entity implements TaskListener {

    protected long id;
    protected BeGodGame game;
    protected String name;
    protected Vector2 position;
    protected int drawPriority;
    protected Sprite aspect;
    protected EntityGroup<?> group;
    protected boolean updated = false;
    protected AIComponent ai;
    public List<Vector2> currentPath;
    protected int lifeTime = 0;
    
    public Entity(BeGodGame game, String name, Vector2 position) {
        this.id = game.getNextID();
        this.game = game;
        this.name = name;
        this.position = new Vector2(position.x, position.y);
        this.ai = new AIComponent(game, this);
        this.ai.setTaskListener(this);
        init();
    }
    
    // Called automatically when created
    public abstract void init();
    
    // Called each game iteration
    public void update(int iteration) {
        lifeTime++;
        ai.think(iteration);
        updated = true;
    }
    
    // Called each frame
    public void render(SpriteBatch batch) {
        if (aspect != null) {
            float size = game.getWorld().getSquareSize();
            aspect.setSize(size, size);
            aspect.setOrigin(0, 0);
            aspect.setPosition(position.x * size, position.y * size);
            aspect.draw(batch);
        }
    }
    
    public void move(Vector2 target) {
        game.getWorld().getSquare(position).remove(this);
        position.x = target.x;
        position.y = target.y;
        game.getWorld().getSquare(position).add(this);
    }
    
    // Must be called before removing all references
    public void destroy() {
        game.getWorld().getSquare(position).remove(this);
    }
    
    // Getters && Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public int getDrawPriority() {
        return drawPriority;
    }

    public Entity setDrawPriority(int drawPriority) {
        game.getWorld().drawPriorityChanged(position);
        this.drawPriority = drawPriority;
        return this;
    }
    
    public EntityGroup<?> getGroup() {
        return group;
    }

    public void setGroup(EntityGroup<?> group) {
        this.group = group;
    }
    
    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }
    
    public long getId() {
        return id;
    }
    
    public AIComponent getAI() {
        return ai;
    }

    public static class DrawPriorityComparator implements Comparator<Entity> {

        @Override
        public int compare(Entity o1, Entity o2) {
            if (o1.getDrawPriority() > o2.getDrawPriority()) {
                return 1;
            }
            
            return -1;
        }
        
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Entity other = (Entity) obj;
        if (id != other.id)
            return false;
        return true;
    }
    
    @Override
    public void onAllTasksFinished() {}
    
}
