package com.wordpress.rasanti.begodfree.framework;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public abstract class GUIScreen {

    private static Map<Class<? extends GUIScreen>,GUIScreen> screens = new HashMap<Class<? extends GUIScreen>,GUIScreen>();
    
    public static <T extends GUIScreen> GUIScreen buildScreen(Game game, Class<? extends GUIScreen> c) {
        GUIScreen screen = screens.get(c);
        if (screen == null) {
            try {
                screen = c.getConstructor(Game.class).newInstance(game);
                screens.put(c, screen);
            } catch (NoSuchMethodException ex) {
                ex.printStackTrace();
            } catch (InvocationTargetException ex) {
                ex.printStackTrace();
            } catch (IllegalAccessException ex) {
                ex.printStackTrace();
            } catch (InstantiationException ex) {
                ex.printStackTrace();
            }
        }
        
        return screen;
    }
    
    protected Game game;
    protected Skin skin;
    
    protected GUIScreen(Game game) {
        this.game = game;
        this.skin = SkinBuilder.buildSkin();
        this.init();
    }
    
    protected abstract void init();
    public abstract void render(SpriteBatch batch);
    public abstract void destroy();
    
}
