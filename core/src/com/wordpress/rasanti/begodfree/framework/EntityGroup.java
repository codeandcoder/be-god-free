package com.wordpress.rasanti.begodfree.framework;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.begodfree.BeGodGame;
import com.wordpress.rasanti.begodfree.BeGodWorld;
import com.wordpress.rasanti.begodfree.utils.EntityUtils;

public abstract class EntityGroup<T extends Entity> {

    protected List<T> entities;
    protected long id;
    protected BeGodGame game;
    protected String name;
    protected Vector2 position;
    
    public EntityGroup(BeGodGame game, String name, Vector2 position) {
        this.id = game.getNextID();
        this.game = game;
        this.name = name;
        this.position = position;
        init();
    }
    
    public void init() {
        entities = new ArrayList<T>();
    }
    
    public abstract void destroy();
    
    public void addAllEntitiesToSquares(BeGodWorld world) {
        for (int i = 0; i < entities.size(); i++) {
            Entity e = entities.get(i);
            if (EntityUtils.isInMap(world.getSizeX(), world.getSizeY(), e.getPosition())
                    && !EntityUtils.hasEntity(world.getSquare((int)e.getPosition().x,(int)e.getPosition().y), e.getClass())) {
                
                world.getSquare((int)e.getPosition().x,(int)e.getPosition().y).add(e);
            }
        }
    }
    
    public void setAllEntitiesToSquares(BeGodWorld world) {
        for (int i = 0; i < entities.size(); i++) {
            Entity e = entities.get(i);
            if (EntityUtils.isInMap(world.getSizeX(), world.getSizeY(), e.getPosition())) {
                world.getSquare((int)e.getPosition().x, (int)e.getPosition().y).clear();
                world.getSquare((int)e.getPosition().x, (int)e.getPosition().y).add(e);
            }
        }
    }
    
    // Getters && Setters
    public void addEntity(T entity) {
        if (entities.add(entity)) {
            entity.setGroup(this);
        }
    }
    
    public void removeEntity(T entity) {
        if (entities.remove(entity)) {
            entity.setGroup(null);
        }
    }
    
    public List<T> getEntities() {
        return entities;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Vector2 getPosition() {
        return position;
    }

}
