package com.wordpress.rasanti.begodfree.framework;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.I18NBundle;

public class InternalFilesLoader extends ResourceLoader {

    private boolean initialized = false;
    private Map<String, Texture> textures = new HashMap<String, Texture>();
    private Map<String, Sound> sounds = new HashMap<String, Sound>();
    private Map<String, Music> musics = new HashMap<String, Music>();
    private I18NBundle bundle;
    
    public InternalFilesLoader(String texturesPath, String soundPath,
            String musicPath, String i18nPath) {
        super(texturesPath, soundPath, musicPath, i18nPath);
    }
    
    @Override
    public void initResources() {
        String initialPath = Gdx.app.getType() == ApplicationType.Desktop ? "./bin/" : "";
        
        if (texturePath != null) {
            FileHandle textureDirectory = Gdx.files.internal(initialPath + texturePath);
            if (textureDirectory.exists()) {
                FileHandle[] textureFiles = textureDirectory.list();
                loadTextures(textureFiles);
            }
        }
        
        if (soundPath != null) {
            FileHandle soundDirectory = Gdx.files.internal(initialPath + soundPath);
            if (soundDirectory.exists()) {
                FileHandle[] soundFiles = soundDirectory.list();
                loadSounds(soundFiles);
            }
        }
        
        if (musicPath != null) {
            FileHandle musicDirectory = Gdx.files.internal(initialPath + musicPath);
            if (musicDirectory.exists()) {
                FileHandle[] musicFiles = musicDirectory.list();
                loadMusics(musicFiles);
            }
        }
        
        if (i18nPath != null) {
            FileHandle baseFileHandle = Gdx.files.internal(initialPath + i18nPath);
            if (baseFileHandle.exists()) {
                bundle = I18NBundle.createBundle(baseFileHandle);
            }
        }
        
        initialized = true;
    }
    
    private void loadTextures(FileHandle[] files) {
        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                loadTextures(files[i].list());
            } else {
                textures.put(files[i].nameWithoutExtension(), new Texture(files[i]));
                Gdx.app.debug("ResourcesLoader", "Texture loaded: " + files[i].nameWithoutExtension());
            }
        }
    }
    
    private void loadSounds(FileHandle[] files) {
        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                loadSounds(files[i].list());
            } else {
                sounds.put(files[i].nameWithoutExtension(), Gdx.audio.newSound(files[i]));
            }
        }
    }
    
    private void loadMusics(FileHandle[] files) {
        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                loadMusics(files[i].list());
            } else {
                musics.put(files[i].nameWithoutExtension(), Gdx.audio.newMusic(files[i]));
            }
        }
    }
    
    @Override
    public Sound getSound(String name) {
        if (!initialized) {
            throwNotInitializedException();
        }
        
        return sounds.get(name);
    }

    @Override
    public Music getMusic(String name) {
        if (!initialized) {
            throwNotInitializedException();
        }
        
        return musics.get(name);
    }

    @Override
    public Texture getTexture(String name) {
        if (!initialized) {
            throwNotInitializedException();
        }
        
        return textures.get(name);
    }

    @Override
    public I18NBundle getI18NBundle(String path) {
        if (!initialized) {
            throwNotInitializedException();
        }
        
        return bundle;
    }

    @Override
    public Texture getNoLoadedTexture(String path) {
        if (path != null) {
            FileHandle file = Gdx.files.internal(path);
            if (file.exists()) {
                return new Texture(file);
            }
        }
        return null;
    }

    @Override
    public void dispose() {
        disposeTextures();
        disposeSounds();
        disposeMusics();
    }
    
    private void disposeTextures() {
        for (Map.Entry<String, Texture> texture : textures.entrySet()) {
            texture.getValue().dispose();
        }
        textures.clear();
    }
    
    private void disposeSounds() {
        for (Map.Entry<String, Sound> sound : sounds.entrySet()) {
            sound.getValue().dispose();
        }
        sounds.clear();
    }
    
    private void disposeMusics() {
        for (Map.Entry<String, Music> music : musics.entrySet()) {
            music.getValue().dispose();
        }
        musics.clear();
    }
    
    private void throwNotInitializedException() {
        throw new RuntimeException("Resources not initialized!");
    }

}
