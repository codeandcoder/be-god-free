package com.wordpress.rasanti.begodfree.pathfinding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.badlogic.gdx.math.Vector2;
import com.wordpress.rasanti.begodfree.BeGodGame;
import com.wordpress.rasanti.begodfree.entities.FreshWater;
import com.wordpress.rasanti.begodfree.entities.SaltWater;
import com.wordpress.rasanti.begodfree.entities.Tree;
import com.wordpress.rasanti.begodfree.entities.Water;
import com.wordpress.rasanti.begodfree.framework.Entity;
import com.wordpress.rasanti.begodfree.utils.EntityUtils;

public class APathfinder {
    
    public static List<Vector2> findPath(BeGodGame game, Entity agent, Vector2 target) {
        Node initialNode = new Node(agent.getPosition(), target);
        List<Vector2> result = new ArrayList<Vector2>();
        List<Node> openNodes = new ArrayList<Node>();
        List<Node> closedNodes = new ArrayList<Node>();
        Node targetNode = new Node(target, target);
        boolean finished = false;
        
        openNodes.add(initialNode);
        while (!finished && openNodes.size() > 0) {
            Node currentNode = openNodes.get(0);
            openNodes = refreshNodes(currentNode, openNodes, closedNodes, target, game);
            openNodes.remove(currentNode);
            if (openNodes.isEmpty()) {
                finished = true;
            } else {
                closedNodes.add(currentNode);
                Collections.sort(openNodes);
                
                if (openNodes.get(0).equals(targetNode)) {
                    result.add(target);
                    while(currentNode.parent != null) {
                        result.add(0, currentNode.getPosition());
                        currentNode = currentNode.parent;
                    }
                    finished = true;
                }
            }
        }

        return result;
    }
    
    private static List<Node> refreshNodes(Node current, List<Node> open, List<Node> closed, Vector2 target, BeGodGame game) {
        List<Node> result = open;
        
        Vector2 cPos = current.getPosition();
        List<Vector2> positionsToCheck = new ArrayList<Vector2>();
        positionsToCheck.add(new Vector2(cPos.x, cPos.y-1));
        positionsToCheck.add(new Vector2(cPos.x, cPos.y+1));
        positionsToCheck.add(new Vector2(cPos.x-1, cPos.y-1));
        positionsToCheck.add(new Vector2(cPos.x-1, cPos.y+1));
        positionsToCheck.add(new Vector2(cPos.x-1, cPos.y-1));
        positionsToCheck.add(new Vector2(cPos.x-1, cPos.y));
        positionsToCheck.add(new Vector2(cPos.x+1, cPos.y));
        positionsToCheck.add(new Vector2(cPos.x+1, cPos.y+1));
        
        for (int i = 0; i < positionsToCheck.size(); i++) {
            Vector2 pos = positionsToCheck.get(i);
            if (EntityUtils.isInMap(game.getWorld().getSizeX(), game.getWorld().getSizeY(), pos)) {
                Node newNode = new Node(pos, target);
                newNode.parent = current;
                if (!closed.contains(newNode)) {
                    int indexOfOpen = open.indexOf(newNode);
                    if (indexOfOpen != -1) {
                        newNode = open.get(indexOfOpen);
                        if (current.calculateG(newNode) < newNode.parent.calculateG(newNode)) {
                            newNode.parent = current;
                        }
                    } else {
                        List<Entity> entities = game.getWorld().getSquare(pos); 
                        boolean obstacle = EntityUtils.isObstacle(entities);
                        if (!obstacle) {
                            result.add(newNode);
                        }
                    }
                }
            }
        }
        
        return result;
    }

}
