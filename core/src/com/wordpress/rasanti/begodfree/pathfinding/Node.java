package com.wordpress.rasanti.begodfree.pathfinding;

import com.badlogic.gdx.math.Vector2;

public class Node implements Comparable<Node> {

    public static final int HV_MOVE_COST = 10;
    public static final int DIAG_MOVE_COST = 14;
    
    public Node parent;
    private Vector2 position;
    private Vector2 target;
    
    public Node(Vector2 position, Vector2 target) {
        this.position = position;
        this.target = target;
    }
    
    public int calculateG(Node target) {
        if (target == null || parent == null) {
            return 0;
        } else if (target.equals(this)) {
            return parent.calculateG(parent.parent);
        }
        
        return parent.calculateG(parent.parent) + costTo(target);
    }
    
    public int calculateH() {
        int xDifference = (int) Math.abs(target.x - position.x);
        int yDifference = (int) Math.abs(target.y - position.y);
        return (xDifference + yDifference) * HV_MOVE_COST;
    }
    
    private int costTo(Node target) {
        if (position.x == target.getPosition().x || position.y == target.getPosition().y) {
            return HV_MOVE_COST;
        }
            
        return DIAG_MOVE_COST;
    }
    
    public Vector2 getPosition() {
        return position;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((position == null) ? 0 : position.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Node other = (Node) obj;
        if (position == null) {
            if (other.position != null)
                return false;
        } else if (!position.equals(other.position))
            return false;
        return true;
    }

    @Override
    public int compareTo(Node o) {
        int myScore = calculateG(this) + calculateH();
        int otherScore = o.calculateG(o) + o.calculateH();
        return myScore - otherScore;
    }
    
}
